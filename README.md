# form_handler

A simple kemal server to take form input from static sites and email that to various accounts.

## Dependencies

The following dependencies likely need to be installed:

	brew install libgc openssl

## Library Path

If you seed the following error:

	ld: library not found for -lssl (this usually means you need to install the development package for libssl)

This should be added to your shell's rc file e.g. `.zshrc`

	export CRYSTAL_LIBRARY_PATH='/usr/local/opt/openssl/lib'
