require "./form_handler/*"

require "yaml"
require "kemal"
require "email"
require "crinja"

config = YAML.parse(File.read("form_handler.yml"))
credentials = YAML.parse(File.read(config["credentials_file"].as_s))
message_text = File.read(config["message_template"].as_s)

post "/" do |env|
  EMail.send(credentials["server"].as_s,
              credentials["port"].as_i,
              use_tls: credentials["use_tls"].raw.as(Bool),
              auth: {credentials["name"].as_s, credentials["password"].as_s}) do

    subject   config["subject"].as_s
    from      config["from"].as_s
    to        config["to"].as_s
    reply_to  env.params.body["email"].as(String)

    message   Crinja.render(message_text, env.params.body.to_h.merge({"time" => Time.now}))
  end
end

Kemal.run
